#include <iostream>
#include "CAnimal.h"

void CAnimal::Voice()
{
	printf("Default class\n");
}

void CDog::Voice()
{
	printf("Woof!\n");
}

void CCat::Voice()
{
	printf("Meow!\n");
}

void CChicken::Voice()
{
	printf("Cluck!\n");
}