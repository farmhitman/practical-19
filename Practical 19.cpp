#include <iostream>
#include "CAnimal.h"

int main()
{
    const int mSize = 3;
    CAnimal* mAnimals[mSize];

    mAnimals[0] = new CDog();
    mAnimals[1] = new CCat();
    mAnimals[2] = new CChicken();

    for (int i = 0; i < mSize; i++) {
        mAnimals[i]->Voice();
    }
}

