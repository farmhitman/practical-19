#pragma once
class CAnimal
{
public:
	virtual void Voice();
};

class CDog : public CAnimal {
public:
	void Voice() override;
};

class CCat : public CAnimal {
public:
	void Voice() override;
};

class CChicken : public CAnimal {
public:
	void Voice() override;
};